package visitor;

import employees.Employee;
import enumes.IEmployeeType;

/**
 * Created by hedgerocker on 2015-10-05.
 */
public abstract class AbstractEmployeeTypeVisitor {

   public abstract Employee get(IEmployeeType type);
}
