package visitor;

import employees.*;
import enumes.EmployeeType4;
import enumes.IEmployeeType;

import java.util.Map;
import java.util.TreeMap;

import static enumes.EmployeeType4.*;

/**
 * Created by hedgerocker on 2015-10-05.
 */
public class EmployeeTypeVisitor extends AbstractEmployeeTypeVisitor {
    private static Map< String, EmployeeType4> map =
            new TreeMap< >();

    static {
        for (EmployeeType4 font : values()) {
            map.put(font.toString(), font);
        }
    }
    @Override
    public Employee get(IEmployeeType type) {
        Employee employee = null;
        switch (map.get(type.toString())) {
            case ENGINEER:
                employee = new Engineer();
                break;
            case MANAGER:
                employee = new Manager();
                break;
            case SALESMAN:
                employee = new Salesman();
                break;
            case DOGGY:
                employee = new Doggy();
                break;
            case KITTY:
                employee = new Kitty();
                break;
        }
        return employee;
    }
}
