package def;

import employees.Employee;
import enumes.EmployeeType2;
import enumes.EmployeeType3;
import enumes.EmployeeType4;
import visitor.EmployeeTypeVisitor;

import java.util.Arrays;

/**
 * Created by hedgerocker on 2015-10-05.
 */
public class Main {
    public static void main(String[] args) {


        final EmployeeTypeVisitor employeeTypeVisitor = new EmployeeTypeVisitor();
        Arrays.asList(EmployeeType2.values()).forEach(e->{
            Employee employee = e.getEmployee(employeeTypeVisitor);
            System.out.println(employee.payAmount());
        });
        Arrays.asList(EmployeeType3.values()).forEach(e->{
            Employee employee = e.getEmployee(employeeTypeVisitor);
            System.out.println(employee.payAmount());
        });
        Arrays.asList(EmployeeType4.values()).forEach(e->{
            Employee employee = e.getEmployee(employeeTypeVisitor);
            System.out.println(employee.payAmount());
        });
    }
}
