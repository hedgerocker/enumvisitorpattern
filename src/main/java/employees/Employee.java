package employees;

/**
 * Created by hedgerocker on 2015-10-05.
 */
public interface Employee {
    Enum getType();
    int payAmount();
}
