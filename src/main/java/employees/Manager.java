package employees;

import enumes.EmployeeType2;

/**
 * Created by hedgerocker on 2015-10-05.
 */
public class Manager extends AbstractEmployee {
    @Override
    public Enum getType() {
        return EmployeeType2.MANAGER;
    }

    @Override
    public int payAmount() {
        return getMonthlySalary() + getBonus();
    }
}
