package employees;

/**
 * Created by hedgerocker on 2015-10-05.
 */
public abstract class AbstractEmployee implements Employee {

    public String toString() {
        return getType().toString();
    }

    public abstract int payAmount();

    public int getMonthlySalary(){
        return 100;
    }

    public int getBonus(){
        return 10;
    }

}
