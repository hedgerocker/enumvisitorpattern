package employees;

import enumes.EmployeeType3;

/**
 * Created by hedgerocker on 2015-10-05.
 */
public class Kitty implements Employee {


    @Override
    public Enum getType() {
        return EmployeeType3.KITTY;
    }

    @Override
    public int payAmount() {
        return 20;
    }
}
