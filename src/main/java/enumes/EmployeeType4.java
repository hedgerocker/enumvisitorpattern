package enumes;

import employees.Employee;
import visitor.AbstractEmployeeTypeVisitor;

/**
 * Created by hedgerocker on 2015-10-05.
 */
public enum EmployeeType4 implements IEmployeeType {
    ENGINEER,
    MANAGER,
    SALESMAN,
    DOGGY,
    KITTY;

    public Employee getEmployee(AbstractEmployeeTypeVisitor visitor) {
        return visitor.get(this);
    }

}
