package enumes;

import employees.Employee;
import visitor.AbstractEmployeeTypeVisitor;

/**
 * Created by hedgerocker on 2015-10-05.
 */
public enum EmployeeType3 implements IEmployeeType {
    DOGGY,
    KITTY;

    public Employee getEmployee(AbstractEmployeeTypeVisitor visitor) {
        return visitor.get(this);
    }

    @Override
    public String toString() {
        return this.name();
    }
}
