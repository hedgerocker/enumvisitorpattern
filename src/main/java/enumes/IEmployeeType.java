package enumes;

/**
 * Created by hedgerocker on 2015-10-05.
 */
public interface IEmployeeType {
    String toString();
}
